package repository;

import entity.Artist;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// to execute this test you must have the Docker application open
@QuarkusTest
public class ArtistRepositoryTest {

    @Inject
    private ArtistRepository artistRepository;

    @Test
    public void createAndFindArtist() throws SQLException {
        Artist artist = new Artist();
        artist.setName("Marco");
        artist.setBio("Where am I?");

        artistRepository.persist(artist);
        assertNotNull(artist.getId());

        artist = artistRepository.findById(artist.getId());
        assertEquals("Marco", artist.getName());
    }
}
