# Quarkus project with 3 databases

This prject represents an application with 3 different way to connect to 3 different databases.

## MySql

The connection with this DB is made with simple POJO class.

## MariaDB

The connection with this DB is made with Hibernate.

## PostgreSql

The connection with this DB is made with Panache.