import entity.Customer;
import io.quarkus.test.TestTransaction;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@QuarkusTest
public class CustomerRepositoryTest {

    @Inject
    private CustomerRepository customerRepository;

    @Test
    @TestTransaction
    public void createAndFindCustomer() {
        Customer customer = new Customer();
        customer.setFirstName("Marco");
        customer.setLastName("Cordoni");
        customer.setEmail("myMail@outlook.it");

        customerRepository.persist(customer);
        assertNotNull(customer.getId());

        customer = customerRepository.findById(customer.getId());
        assertEquals("Marco", customer.getFirstName());
    }
}
