package entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.*;

import java.time.Instant;

@Entity
@Table(name = "PURCHASE_ORDER_LINES")
public class OrderLine extends PanacheEntity {

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "item_fk")
    public Item item;

    @Column(nullable = false)
    public Integer quantity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "purchase_order_fk")
    @JsonbTransient
    public PurchaseOrder purchaseOrder;

    @Column(name = "CREATED_DATE", nullable = false)
    public Instant createdDate = Instant.now();

}