package entity;

public enum Language {
    ENGLISH, FRENCH, SPANISH, PORTUGUESE, RUSSIAN, CHINESE, INDIAN, GERMAN, JAPANESE
}