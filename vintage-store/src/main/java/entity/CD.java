package entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class CD extends Item {

    @Column(name = "MUSIC_COMPANY")
    public String musicCompany;

    @Column(name = "GENRE", length = 100)
    public String genre;

    @OneToMany(mappedBy = "cd", cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval = true)
    public List<Track> tracks = new ArrayList<>();

}
