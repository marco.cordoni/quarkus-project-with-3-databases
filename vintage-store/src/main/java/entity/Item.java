package entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.inject.Inject;
import jakarta.persistence.*;
import repository.ArtistRepository;

import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Table(name = "ITEM")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Item extends PanacheEntity {

    @Column(name = "TITLE", length = 100, nullable = false)
    public String title;
    @Column(name = "DESCRIPTION", length = 3000)
    public String description;
    @Column(name = "PRICE", nullable = false)
    public BigDecimal price;
    @Column(name = "CREATED_DATE", nullable = false)
    public Instant createdDate = Instant.now();

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "ARTIST_FK")
    public Artist artist;

}
