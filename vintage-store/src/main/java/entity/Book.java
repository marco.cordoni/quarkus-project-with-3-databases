package entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
public class Book extends Item {

    @Column(name = "ISBN", length = 15)
    public String isbn;
    @Column(name = "N_OF_PAGES")
    public int nOfPages;
    @Column(name = "PUBLICATION_DATE")
    public LocalDate publicationDate;
    @Column(name = "LANGUAGE", length = 20)
    @Enumerated(EnumType.STRING)
    public Language language;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "PUBLISHER_FK")
    public Publisher publisher;
}
