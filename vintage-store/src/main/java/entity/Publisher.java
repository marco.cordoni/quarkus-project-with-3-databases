package entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.*;
import org.hibernate.annotations.Columns;

import java.time.Instant;
import java.util.List;
import java.util.Optional;


/**
 * Example JPA entity defined as a Panache Entity.
 * An ID field of Long type is provided, if you want to define your own ID field extends <code>PanacheEntityBase</code> instead.
 *
 * This uses the active record pattern, you can also use the repository pattern instead:
 * .
 *
 * Usage (more example on the documentation)
 *
 * {@code
 *     public void doSomething() {
 *         MyEntity entity1 = new MyEntity();
 *         entity1.field = "field-1";
 *         entity1.persist();
 *
 *         List<MyEntity> entities = MyEntity.listAll();
 *     }
 * }
 */
@Entity
@Table(name = "PUBLISHER")
public class Publisher extends PanacheEntity {

    @Column(name = "NAME", length = 50, nullable = false)
    public String name;

    @Column(name = "CREATED_DATE", nullable = false)
    public Instant createDate = Instant.now();

    public static Optional<Publisher> findByName(String name) {
        return Publisher.find("name", name).firstResultOptional();
    }

    public static List<Publisher> findContainName(String name) {
        return Publisher.list("name like ?1", "%" + name + "%");
    }

}
